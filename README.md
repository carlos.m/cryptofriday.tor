# tor@cryptofriday

Coisas sobre a oficina Tor Everywhere na Cryptofriday #4. 

![Cenário típico](./tor.svg)

## Exemplos de configuração do client ( diretório `tor`):

* torrc.proxy : Apenas para navegar usando a rede tor.
* torrc.relay : Uma relay é um nó que ajuda a rede roteando tráfego por ele.
* torrc.dist : Arquivo distribuído junto da instalação, com configurações típicas e vários exemplos.

## Roteador wifi com Tor

Configurações extras para um roteador baseado em um sistema Arch Linux Arm em um Raspberry Pi 3. 

Observe que usamos apenas IPv4.

Certamente, apertando os devidos parafusos (e.g.: nomes de interfaces de rede), funcionaria em qualquer lugar.

Dependências:

* `dnsmasq`
* `hostapd`
* `iptables`
* `tor` (opcionalmente: `nyx`)

Arquivos (o `etc` aqui corresponde a um `/etc`) e o que eles fazem:

* `etc/tor/torrc` : Configuração do client `tor`.
* `etc/dnsmasq.conf` : `dnsmasq` é um servidor DNS e DHCP, mas aqui apenas usado para este último.
* `etc/systemd/system/openwifi.service` : Serviço simples para pré-configuração da placa de rede wifi. 
* `etc/hostapd/hostapd.conf` : `hostapd` é um hotspot wifi, mas feito em software.
* `etc/sysctl.conf` : Uma configuração necessária do kernel.
* `etc/iptables/iptables.rules` : Regras de firewall que fazem forward de todas as requisições DNS da rede wifi para o resolver do client tor, e forward de todo o resto para a `TransPort`.

## Referências

* Motivação original: https://web.archive.org/web/20160428235033/https://tamcore.eu/anonymen-wlan-access-point-mit-archlinux-und-tor-errichten/
* Ask the `man`: `man tor`
* Tor source code: https://gitweb.torproject.org/tor.git/
* Documentation (in transition): https://2019.www.torproject.org/docs/documentation.html.en
* ArchWiki: https://wiki.archlinux.org/index.php/Tor

## Or, the other way

Tor browser: https://www.torproject.org/download/

Orbot, tor for Android: https://play.google.com/store/apps/details?id=org.torproject.android


#!/bin/bash

if [ ! "$( whoami )" == 'root' ] ; then 
  echo 'Must be run as root.'
  exit 1
fi

# Maybe there's an easy way to automatically find out which one is which?
# https://en.wikipedia.org/wiki/Consistent_Network_Device_Naming
ethernet='enp0s25'
wifi='wlp3s0'

dnsmasq_cfg="/tmp/dnsmasq.cfg"
dnsmasq_leases="/tmp/dnsmasq.leases"

ip link set up dev "${ethernet}"
ip addr add 192.168.150.1/24 dev "${ethernet}"

sysctl net.ipv4.ip_forward=1

iptables -t nat -A POSTROUTING -o "${wifi}" -j MASQUERADE
iptables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i "${ethernet}" -o "${wifi}" -j ACCEPT
iptables -I INPUT -p udp --dport 67 -i "${ethernet}" -j ACCEPT
iptables -I INPUT -p udp --dport 53 -s 192.168.150.0/24 -j ACCEPT
iptables -I INPUT -p tcp --dport 53 -s 192.168.150.0/24 -j ACCEPT

cat << EOF > "${dnsmasq_cfg}"
filterwin2k
dhcp-range=192.168.150.2,192.168.150.10,15m
interface=${ethernet}
dhcp-leasefile=${dnsmasq_leases}
log-queries
log-dhcp
EOF

dnsmasq -d -C "${dnsmasq_cfg}"
